extends Node2D

var direction
var speed = 4
var speed_factor = 10
var rotation_speed = 1
var rotation_dir = 1

func _process(delta):

#	return

	var velocity = direction * speed * speed_factor
	position += velocity * delta

	rotation += rotation_dir * rotation_speed *  delta

	if position.x > 960:
		position.x = 0

	if position.x < 0:
		position.x = 960

	if position.y > 540:
		position.y = 0

	if position.y < 0:
		position.y = 540



func _ready():
	var ANGLE = floor(rand_range(0, 360))
	direction = Vector2(cos(deg2rad(ANGLE-90)), sin(deg2rad(ANGLE-90)))