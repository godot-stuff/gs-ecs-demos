extends Node

export (bool) var USE_ECS = true
export (int) var MAX_ENTITIES = 100


export (PackedScene) var p_entity
export (PackedScene) var p_node

var entities = 0

func _process(delta):

	$CanvasLayer/Container/Info.text = "fps: %s \nentities: %s \n" % [Engine.get_frames_per_second(), entities]


	if USE_ECS:
		ECS.update()

func _on_Timer_timeout():

	if entities > MAX_ENTITIES:
		return

	var e
	if USE_ECS:
		e = p_entity.instance()
	else:
		e = p_node.instance()

	e.position = Vector2(480, 270)
	add_child(e)
	entities += 1
