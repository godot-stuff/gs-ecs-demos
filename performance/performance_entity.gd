extends "res://gs_ecs/entity.gd"

func on_ready():

	Logger.trace("[performance_entity] on_ready")

	var _vel = get_component("velocity")
	var _rot = get_component("rotating")

	var ANGLE = floor(rand_range(0, 360))
	_vel.direction = Vector2(cos(deg2rad(ANGLE-90)), sin(deg2rad(ANGLE-90)))

	var dir = [1, -1]
	_rot.direction = dir[floor(randi() % 2)]
	_rot.rotation = floor(randi() % 360)
