extends Node

"""
	Class: Asteroids Singleton

	Provides a Singleton for the Game Systems to
	communicate.

	Remarks:

"""

# when a player is destroyed
signal player_created()
signal player_destroyed()

# when alien destroyed
signal alien_created()
signal alien_destroyed()

# when an asteroid is created or destroyed
signal asteroid_destroyed(asteroid)
signal asteroid_created(asteroid)

# when the game state changes
signal game_state_changed(state)

# when the score has changedw
signal score_changed(score)

# game variables
var lives = 3
var score = 0
var hiscore = 0
var rounds = 0

var state = GAME_STATE_NONE


enum GAME_STATES {
	GAME_STATE_NONE,
	GAME_STATE_ROUND,
	GAME_STATE_RESET,
	GAME_STATE_RUN,
	GAME_STATE_READY,
	GAME_STATE_OVER,
	GAME_STATE_PAUSE,
	GAME_STATE_TITLE,
	GAME_STATE_WIN,
	GAME_STATE_SCORES
}

enum PLAYER_STATES {
	PLAYER_STATE_NONE,
	PLAYER_STATE_THRUSTING,
	PLAYER_STATE_ROTATING,
	PLAYER_STATE_DEAD
}


func add_score(score):
	self.score += score
	emit_signal("score_changed", self.score)

	if self.score > hiscore:
		hiscore = self.score


func change_state(state):
	self.state = state
	emit_signal("game_state_changed", self.state)


