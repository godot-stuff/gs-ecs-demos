extends Node

# when this asteroid is broken, what is the next asteroid
export (PackedScene) var NEXT_ASTEROID
# how many asteroids to create
export (int) var NEXT_COUNT

var next_asteroid = null
var next_count = 0


func _ready():

	if NEXT_ASTEROID:	next_asteroid = NEXT_ASTEROID
	if NEXT_COUNT:		next_count = NEXT_COUNT
