extends Node


export (int) var THRUST_MAX = 10
export (int) var THRUST_FACTOR = 10
export (int) var THRUST_FRAMES = 20

var thrusting = false
var thrust = 0
var thrust_max = 10
var thrust_factor = 10
var thrust_frames = 20
var thrust_frame = 0


func _ready():

	if THRUST_MAX:		thrust_max = THRUST_MAX
	if THRUST_FACTOR:	thrust_factor = THRUST_FACTOR
	if THRUST_FRAMES:	thrust_frames = THRUST_FRAMES