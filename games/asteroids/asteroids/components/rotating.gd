
extends Node

"""
	Component: Rotating

	Refers to an Object that is continually Rotating, or
	can be rotated autonomously.

	Remarks:

		For entites that need to be rotated by the
		Player, try using the Rotatable component.
"""

export (int) var SPEED = 1
export (int) var SPEED_FACTOR = 1


var direction = 1
var speed = 1
var speed_factor = 1
var rotation = 0


func _ready():

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR

