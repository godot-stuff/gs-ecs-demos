extends Node

export (float) var COOLDOWN = 1.0

# should shoot next turn
var shooting = false
# can the entity shoot
var can_shoot = false
# shooting cooldown amount
var shooting_cooldown = 1.0
# direction of next shot
var shooting_direction = Vector2()


func _ready():

	if COOLDOWN:	shooting_cooldown = COOLDOWN
