extends Node

"""
	Script: Main Asteroid Game Script

	This Script controls most of the State of
	the Game.

	Remarks:


"""


const AsteroidGigantic = preload("res://asteroids/entities/asteroid_gigantic.tscn")
const AsteroidHuge = preload("res://asteroids/entities/asteroid_huge.tscn")
const AsteroidBig = preload("res://asteroids/entities/asteroid_big.tscn")
const AsteroidMedium = preload("res://asteroids/entities/asteroid_medium.tscn")
const AsteroidSmall = preload("res://asteroids/entities/asteroid_small.tscn")
const Player = preload("res://asteroids/entities/player.tscn")
const Shield = preload("res://asteroids/entities/shield.tscn")
const ShieldComponent = preload("res://asteroids/components/shield.gd")
const Alien = preload("res://asteroids/entities/alien.tscn")


export (bool) var DEBUG = true
export (int) var ROUND = 1

var asteroid_count = 0
var alien_spawn_timer = 2
var game_state = Asteroids.GAME_STATE_NONE
var player
var shield
var alien

func _create_asteroids(count, type = 0):
	Logger.trace("[asteroids_game] _create_asteroids")

	# display some asteroids
	var _count = 0

	while _count < count:

		var x = floor(randi() % 1920)

		if x > 600 and x < 1020:
			continue

		var y = floor(randi() % 1080)

		var _asteroid

		if type == 0:
			_asteroid = AsteroidGigantic.instance()

		if type == 1:
			_asteroid = AsteroidHuge.instance()

		if type == 2:
			_asteroid = AsteroidBig.instance()

		if type == 3:
			_asteroid = AsteroidMedium.instance()

		if type == 4:
			_asteroid = AsteroidSmall.instance()

		_asteroid.position = Vector2(x, y)
		$Entities.add_child(_asteroid)
		_count += 1


func _create_round():
	Logger.trace("[asteroids_game] _create_round")

	if Asteroids.rounds > 0:
		_create_asteroids(1, 1)

	if Asteroids.rounds > 1:
		_create_asteroids(1, 1)

	if Asteroids.rounds > 3:
		_create_asteroids(1, 1)

	if Asteroids.rounds > 5:
		_create_asteroids(1, 3)

	if Asteroids.rounds > 7:
		_create_asteroids(1, 4)

	if Asteroids.rounds > 9:
		_create_asteroids(1, 0)

	if Asteroids.rounds > 11:
		_create_asteroids(1, 1)

	if Asteroids.rounds > 13:
		_create_asteroids(1, 2)

	if Asteroids.rounds > 15:
		_create_asteroids(1, 3)

	if Asteroids.rounds > 17:
		_create_asteroids(1, 4)

	if Asteroids.rounds > 19:
		_create_asteroids(1, 1)

	if Asteroids.rounds > 21:
		_create_asteroids(1, 2)

	if Asteroids.rounds > 23:
		_create_asteroids(1, 0)


func _debug():

	if !DEBUG:
		return

	if not DEBUG:
		return

	var debug_text = \
	"""entities: %s
	fps: %s
	asteroids: %s
	score: %s
	lives: %s
	state: %s
	round: %s
	"""

	$DEBUG/Container/Label.text = debug_text % [
		ECS.entities.size(),
		Engine.get_frames_per_second(),
		asteroid_count,
		Asteroids.score,
		Asteroids.lives,
		Asteroids.state,
		Asteroids.rounds
	]


func _game_over():
	Logger.trace("[asteroids_game] _game_ready")

	$UI/Over.show()

	# remove player
	if player:
		ECS.remove_entity(player)
		player = null

func _game_ready():
	Logger.trace("[asteroids_game] _game_ready")

	# show ready
	$UI/Ready.show()

	# create player entity
	player = Player.instance()
	$Entities.add_child(player)
	player.position = Vector2(960, 540)

	# add shield
	shield = Shield.instance()
	var _sc = ShieldComponent.new()
	_sc.name = "Shield"
	player.add_child(shield)
	player.add_component(_sc)
	$ShieldTimer.start()


	for i in range(3):

		$Tween.interpolate_property($UI/Ready/Label, "custom_colors/font_color", ColorN("white", 1.0), ColorN("white", 0.0), 0.75, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		yield($Tween, "tween_completed")

	# enter run mode
	Asteroids.change_state(Asteroids.GAME_STATE_RUN)


# reset the game
func _game_reset():
	Logger.trace("[asteroids_game] _game_reset")

	# reset game counters
	Asteroids.lives = 3
	Asteroids.score = 0
	Asteroids.rounds = 1

	if ROUND:	Asteroids.rounds = ROUND

	$DEBUG/Container/Label.text = ""


# prepare next round
func _game_round():
	Logger.trace("[asteroids_game] _game_round")

	asteroid_count = 0

	# remove existing asteroids
	for entity in $Entities.get_children():
		if entity.name.to_lower().find("asteroid") >= 0:
			ECS.remove_entity(entity)

	# remove player
	_remove_player()

	# show ready
	$UI/Round.show()
	$UI/Round/Label.text = "round %s" % Asteroids.rounds

	# create some stuff
	_create_round()

	# animate round # banner
	for i in range(3):

		$Tween.interpolate_property($UI/Round/Label, "custom_colors/font_color", ColorN("white", 1.0), ColorN("white", 0.0), 0.75, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		yield($Tween, "tween_completed")

	# enter ready mode
	Asteroids.change_state(Asteroids.GAME_STATE_READY)


# run the game
func _game_run():
	Logger.trace("[asteroids_game] _game_run")

	$AlienTimer.start()


func _game_title():
	Logger.trace("[asteroids_game] _game_title")

	# reset game
	_game_reset()

	# show the title
	$UI/Title.show()


func _game_win():

	$UI/Win.show()

	# remove player
	_remove_player()


func _init():
	Logger.trace("[asteroids_game] _init")
	randomize()


func _on_alien_destroyed():
	Logger.trace("[asteroids_game] _on_alien_destroyed")
	$AlienTimer.start()

func _on_asteroid_created(asteroid):
	Logger.trace("[asteroids_game] _on_asteroid_created")
	asteroid_count += 1


func _on_asteroid_destroyed(asteroid):
	Logger.trace("[asteroids_game] _on_asteroid_destroyed")
	asteroid_count -= 1

	# no more asteroids left
	if asteroid_count < 1:

		Asteroids.rounds += 1

		if Asteroids.rounds > 25:
			Asteroids.change_state(Asteroids.GAME_STATE_WIN)
			return

		Asteroids.change_state(Asteroids.GAME_STATE_ROUND)



func _on_game_state_changed(state):
	Logger.trace("[asteroids_game] _on_game_state_changed")

	for child in $UI.get_children():
		child.hide()


	match state:

		Asteroids.GAME_STATE_NONE:
			Asteroids.change_state(Asteroids.GAME_STATE_TITLE)

		Asteroids.GAME_STATE_ROUND:
			_game_round()

		Asteroids.GAME_STATE_RESET:
			_game_reset()
			Asteroids.change_state(Asteroids.GAME_STATE_ROUND)

		Asteroids.GAME_STATE_TITLE:
			_game_title()

		Asteroids.GAME_STATE_READY:
			_game_ready()

		Asteroids.GAME_STATE_RUN:
			_game_run()

		Asteroids.GAME_STATE_PAUSE:
			pass

		Asteroids.GAME_STATE_OVER:
			_game_over()

		Asteroids.GAME_STATE_SCORES:
			pass

		Asteroids.GAME_STATE_WIN:
			_game_win()


func _on_player_destroyed():
	Logger.trace("[asteroids_game] _on_player_destroyed")
	Asteroids.lives -= 1
	Logger.info("- player has %s lives remaining" % [Asteroids.lives])
	if Asteroids.lives > 0:
		ECS.remove_entity(player)
		Asteroids.change_state(Asteroids.GAME_STATE_READY)
		return

	Asteroids.change_state(Asteroids.GAME_STATE_OVER)


func _on_AlienTimer_timeout():
	alien = Alien.instance()
	$Entities.add_child(alien)
	alien.position = Vector2(-100, randi() % 880 + 100)
	alien.get_component("velocity").direction = Vector2(1, 0)
	Asteroids.emit_signal("alien_created")


func _on_Play_pressed():
	Logger.trace("[asteroids_game] _on_Play_pressed")

	Asteroids.change_state(Asteroids.GAME_STATE_RESET)


func _on_ShieldTimer_timeout():
	player.remove_component("shield")
	ECS.remove_entity(shield)


func _process(delta):

	_debug()

	ECS.update("common")

	match Asteroids.state:

		Asteroids.GAME_STATE_RUN:
			ECS.update("run")

		Asteroids.GAME_STATE_READY:
			ECS.update("run")

	$GAME/Scores/Score.text = str(Asteroids.score)
	$GAME/Scores/HiScore.text = str(Asteroids.hiscore)


func _remove_player():
	Logger.trace("[asteroids_game] _remove_player")
	if player:
		ECS.remove_entity(player)
		player = null


func _ready():
	Logger.trace("[asteroids_game] _ready")
	Asteroids.connect("asteroid_created", self, "_on_asteroid_created")
	Asteroids.connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
	Asteroids.connect("game_state_changed", self, "_on_game_state_changed")
	Asteroids.connect("player_destroyed", self, "_on_player_destroyed")
	Asteroids.connect("alien_destroyed", self, "_on_alien_destroyed")
	Asteroids.change_state(Asteroids.GAME_STATE_NONE)


