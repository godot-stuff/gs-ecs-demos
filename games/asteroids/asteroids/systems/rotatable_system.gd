extends "./system.gd"


func on_process(entities, delta):

	for entity in entities:

		var _rot = entity.get_component("rotatable")

		entity.rotation += _rot.rotation * _rot.speed * delta

		#	reset rotation

		_rot.rotation = 0

