extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var _thr = entity.get_component("thrust")
		var _vel = entity.get_component("velocity")

		if not _thr.thrusting:
			_thr.thrust_frame = 0
			_thr.thrust = 0
			return

		_thr.thrust_frame -= 1

		if _thr.thrust_frame < 0:
			_thr.thrust_frame = _thr.thrust_frames
			_thr.thrust += 1

		if _thr.thrust > _thr.thrust_max:
			_thr.thrust = _thr.thrust_max

#		_vel.velocity = Vector2(1 * _vel.speed * _vel.speed_factor, 0).rotated(entity.rotation)
		_vel.velocity = Vector2(_thr.thrust * _thr.thrust_factor, 0).rotated(entity.rotation)
