extends "./system.gd"


export (bool) var USE_VIEWPORT = true
export (Rect2) var BOUNDRY = Rect2()
export (Rect2) var MARGIN = Rect2()

onready var bex = BOUNDRY.end.x
onready var bey = BOUNDRY.end.y
onready var bpx = BOUNDRY.position.x
onready var bpy = BOUNDRY.position.y


func on_process(entities, delta):

	for entity in entities:

		var pos = entity.position
		var ent = entity

		if pos.x > bex:
			ent.position.x = bpx
			return

		if pos.x < bpx:
			ent.position.x = bex
			return

		if pos.y > bey:
			ent.position.y = bpy
			return

		if pos.y < bpy:
			ent.position.y = bey
			return

func on_after_add():

#	gecs.add_system(self, ["position"])

	if USE_VIEWPORT:
		BOUNDRY = Rect2(0, 0, get_viewport().get_visible_rect().size.x, get_viewport().get_visible_rect().size.y)
		MARGIN = Rect2(5, 5, 5, 5)
