extends "../system.gd"

func on_process(entities, delta):

	for entity in entities:

		var _laser = entity.get_component("laser")

		var o = entity.get_overlapping_areas()

		if o.size() > 0:
			var a = o[0]
			Logger.debug("- laser collided with %s" % [a.name])
			if a.name.find("Asteroid") >= 0:
				a.get_component("breaks").broken = true
				entity.position = Vector2(10000, 10000)
				entity.enabled = false
				ECS.remove_entity(entity)
				Asteroids.add_score(5)

			# don't destroy if hitting shield and this is player laser
			if a.name.find("Shield") >= 0:
				Logger.debug("- laser type is %s" % [_laser.type])
				if _laser.type == "player":
					Logger.debug("- laser hit player shield, ignoring")
					return

			# destroy quietly
			ECS.remove_entity(entity)