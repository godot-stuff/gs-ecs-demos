extends "../system.gd"

func on_process(entities, delta):

	for entity in entities:

		var _player_state = entity.get_component("playerstate")

		match _player_state.state:

			Asteroids.PLAYER_STATE_NONE:
				pass

			Asteroids.PLAYER_STATE_DEAD:
				Asteroids.emit_signal("player_destroyed")
				_player_state.state = Asteroids.PLAYER_STATE_NONE