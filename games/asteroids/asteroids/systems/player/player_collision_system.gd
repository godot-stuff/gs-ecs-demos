extends "../system.gd"

func on_process(entities, delta):

	for entity in entities:

		var _player_state = entity.get_component("playerstate")
		var _objs = entity.get_overlapping_areas()

		if _objs.size() > 0:

			# if we have shield ignore
			if entity.has_component("shield"):
				return

			# get first object collided with
			var _obj = _objs[0]
			Logger.debug("- player collided with %s" % [_obj.name])

			# ignore if player laser and hit player
			if _obj.name.find("Laser") >= 0:
				var _laser = _obj.get_component("laser")
				Logger.debug("- laser type is %s" % [_laser.type])
				if _laser.type == "player":
					Logger.debug("- player laser hit player (tee hee),  ignore")
					return


			_player_state.state = Asteroids.PLAYER_STATE_DEAD

