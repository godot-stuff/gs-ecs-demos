extends "../system.gd"

const Laser = preload("res://asteroids/entities/laser.tscn")

func on_process(entities, delta):

	for entity in entities:

		if Input.is_action_just_pressed("fire"):
			var l = Laser.instance()
			l.rotation = entity.rotation
			l.position = entity.position + Vector2(55, 0).rotated(entity.rotation)
			add_child(l)
			var _laser = l.get_component("laser")
			_laser.type = "player"
			# add player speed for adjustment
			var _velocity = l.get_component("velocity")
			_velocity.speed += entity.get_component("velocity").speed
