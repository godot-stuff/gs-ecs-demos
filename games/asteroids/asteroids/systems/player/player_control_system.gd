extends "../system.gd"

func on_process(entities, delta):

	for entity in entities:

		var thrust = entity.get_component("thrust")
		var velocity = entity.get_component("velocity")
		var rotatable = entity.get_component("rotatable")

		var _rotation = 0
		var _velocity = Vector2()

		if Input.is_action_pressed("right"):
			_rotation += 1

		if Input.is_action_pressed("left"):
			_rotation -= 1

		if Input.is_action_pressed("up"):
			thrust.thrusting = true

		if Input.is_action_just_released("up"):
			thrust.thrusting = false

		rotatable.rotation += _rotation
		velocity.velocity += _velocity
