extends "./system.gd"

var player

func on_process(entities, delta):

	for entity in entities:

		var _debug = entity.get_component("debug")

		_debug.rect_position = player.global_position + Vector2(50, 0)

		var _text = \
		"""id: %s
		name: %s
		thrusting: %s
		thrust: %s
		velocity: %s
		next_velocity: %s
		length: %s
		"""

		_debug.text = _text % [
			player.id,
			player.name,
			player.get_component("thrust").thrusting,
			player.get_component("thrust").thrust,
			player.get_component("velocity").velocity.round(),
			player.get_component("friction").next_velocity.round(),
			player.get_component("velocity").velocity.length()
		]


func _ready():
	player = get_tree().get_root().find_node("Player", true, false)
