extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var _fri = entity.get_component("friction")
		var _vel = entity.get_component("velocity")

		# set velocity to the last known velocity
		_vel.velocity += _fri.next_velocity
		_vel.velocity = _vel.velocity.clamped(450)

		# update velocity with applied friction
		_fri.next_velocity = _vel.velocity * 0.99

