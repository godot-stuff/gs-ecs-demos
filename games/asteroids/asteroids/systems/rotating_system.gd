extends "./system.gd"

func on_process(entities, delta):
	for entity in entities:
		var _rot = entity.get_component("rotating")
#		var _rot = ECS.component_entities["rotating"][entity.get_instance_id()]
		_rot.rotation += _rot.direction * _rot.speed * _rot.speed_factor * delta
		entity.rotation = _rot.rotation
