extends "./system.gd"

func on_process(entities, delta):

	for entity in entities:

		var _breaks = entity.get_component("breaks")

		if _breaks.broken:
			Logger.debug("- asteroid broken")

			var _asteroid = entity.get_component("asteroid")

			if _asteroid.next_asteroid:
				for i in range(_asteroid.next_count):
					var _next = _asteroid.next_asteroid.instance()
					_next.position = entity.position
					entity.get_parent().add_child(_next)

			entity.position = Vector2(10000, 10000)
			entity.enabled = false
			ECS.remove_entity(entity)

			Asteroids.emit_signal("asteroid_destroyed", entity)

