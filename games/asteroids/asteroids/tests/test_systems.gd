extends Node

func _process(delta):
	ECS.update()

	var debug_text = \
	"""entities: %s
	fps: %s
	"""

	$Debug/Container/Label.text = debug_text % [
		ECS.entities.size(),
		Engine.get_frames_per_second(),
	]


func _ready():
	ECS.rebuild()
	Logger.logger_level = Logger.LEVEL_ERROR