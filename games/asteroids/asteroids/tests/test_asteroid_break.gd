extends Node


export (PackedScene) var Asteroid = preload("res://asteroids/entities/asteroid_big.tscn")


func _create_asteroid():
	var _a = Asteroid.instance()
	_a.position = Vector2(600, 270)
	var _n = _a.find_node("Components").find_node("Moving")
	_n.remove_and_skip()
	add_child(_a)

func _on_asteroid_created(asteroid):
	# attach timer to destory asteroid after two seconds
	Logger.info("- created " + asteroid.name)
	if not asteroid.name.find("AsteroidBig") >= 0:
		Logger.info("- setting for destruction")
		asteroid.collision_layer = 0
		$Tween.interpolate_callback(self, 10, "_on_destroy_asteroid", asteroid)
		$Tween.start()


func _on_destroy_asteroid(asteroid):
	ECS.remove_entity(asteroid)


func _on_asteroid_destroyed(asteroid):
	if asteroid.name.find("AsteroidBig") >= 0:
		Logger.info("- asteroid hit")
		$Tween.interpolate_callback(self, 3, "_create_asteroid")


func _on_timer():
	pass

func _process(delta):
	ECS.update()
	$DEBUG/Container/Label.text = "entities: %s" % [ECS.entities.size()]


func _ready():
	ECS.rebuild()
	Asteroids.connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
	Asteroids.connect("asteroid_created", self, "_on_asteroid_created")
	_create_asteroid()

func _init():
	randomize()