extends Node

export (PackedScene) var ENTITY = null

const Shield = preload("res://asteroids/entities/shield.tscn")
const ShieldComponent = preload("res://asteroids/components/shield.gd")

var shield

func _on_player_destroyed():
	Logger.info("- player destroyed")
	_shield_toggle()


func _shield_toggle():

	if shield:
		ECS.remove_entity(shield)
		$Player.remove_component("shield")
		shield = null
		return

	shield = Shield.instance()
	var _shield_component =	ShieldComponent.new()
	_shield_component.name = "Shield"
	$Player.add_child(shield)
	$Player.add_component(_shield_component)



func _on_Timer_timeout():

	# spawn entity
	var _entity = ENTITY.instance()
	_entity.position = $Spawn.position

	# rotate to face player
	_entity.rotation_degrees = 180

	add_child(_entity)


func _on_Shield_pressed():
	_shield_toggle()

func _process(delta):
	ECS.update()


func _ready():
	Asteroids.connect("player_destroyed", self, "_on_player_destroyed")
	_shield_toggle()

