extends "./entity.gd"

func on_ready():
	var _vel = get_component("velocity")
	_vel.direction = Vector2(1, 0).rotated(self.rotation)


func _on_Timer_timeout():
	ECS.remove_entity(self)
