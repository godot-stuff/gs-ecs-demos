
class_name LaserPool

# Prefix to use when adding objects to the scene (becomes "undefined_1, undefined_2, etc")
var prefix setget no_setter, get_prefix

# Pool size on initialization
var size setget no_setter, get_size

# Preloaded scene resource
var scene setget no_setter, get_scene

# Dictionary of "active" objects currently in-use.
# Using a dictionary for fast lookup/deletion
var active = {} setget no_setter, no_getter

# Array of "available" objects currently active for use
var available = [] setget no_setter, no_getter

# Constructor accepting pool size, prefix and scene
func _init(size_, prefix_, scene_):
	size = int(size_)
	prefix = str(prefix_)
	scene = scene_
	init()

# Expand the total pool size by the number of size objects.
# For example, if passed 2, we will instantiate 2 new objects and add to the available pool.
func init():
	Logger.trace("[laser_pool] init")
	# If scene has not been set, just return
	if scene == null:
		return

	for i in range(size):
		var s = scene.instance()
		s.set_name(prefix + "_" + str(i))
		available.push_back(s)
		
	hide()

func no_setter(value):
	return

func no_getter():
	return

func get_prefix():
	return prefix

func get_size():
	return size

func get_scene():
	return scene

func get_active_size():
	return active.size()

func get_available_size():
	return available.size()

# Get the first available object and make it active, 
# adding the object to the active pool and removing 
# from available pool
func get_first_available():
	Logger.trace("[laser_pool] get_first_available")
	var ds = available.size()
	if ds > 0:
		var o = available[ds - 1]
#		if !o.available: return null

		var n = o.get_name()
		active[n] = o
		available.pop_back()
#		o.available = false
		o.set_pause_mode(0)
		o.show()
		return o

	return null

# Get the first active object. Does not affect / change the object's available value
func get_first_active():
	Logger.trace("[laser_pool] get_first_active")
	if active.size() > 0:
		return active.values()[0]

	return null

# Convenience method to kill all active objects managed by the pool
func kill_all():
	Logger.trace("[laser_pool] kill_all")
	for target in active.values():
#		i.kill()
		set_unavailable(target)

# Attach all objects managed by the pool to the node passed
func add_to_node(node):
	Logger.trace("[laser_pool] add_to_node")
	for i in active.values():
		node.add_child(i)

	for i in available:
		node.add_child(i)

# Convenience method to show all objects managed by the pool
func show():
	for i in active.values():
		i.show()

	for i in available:
		i.show()

# Convenience method to hide all objects managed by the pool
func hide():
	for i in active.values():
		i.hide()

	for i in available:
		i.hide()

#
func set_unavailable(target):
	Logger.trace("[laser_pool] set_unavailable")
	# Get the name of the target object that was killed
	var name = target.get_name()

	# Remove the killed object from the active pool
	active.erase(name)

	# Add the killed object to the available pool, now active for use
	available.push_back(target)

	target.set_pause_mode(1)
	
	target.position = Vector2.ZERO
	target.get_component("velocity").direction = Vector2.ZERO
	target.hide()

