extends Node
class_name Intruder


export var VERTICAL_SPEED: float = 1
export var HORIZONTAL_SPEED: float = 1


var facing_left = true
var vspeed: float
var hspeed: float
var shoot_dir : float = 0.0


func _ready() -> void:

	if VERTICAL_SPEED:		vspeed = VERTICAL_SPEED
	if HORIZONTAL_SPEED:	hspeed = HORIZONTAL_SPEED