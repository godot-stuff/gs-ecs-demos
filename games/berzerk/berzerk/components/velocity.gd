extends Component
class_name Velocity

export var SPEED = 5
export var SPEED_FACTOR = 10

var speed = 1
var speed_factor = 10
var direction : Vector2 = Vector2.ZERO
var velocity : Vector2 = Vector2.ZERO

func _ready():

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR
