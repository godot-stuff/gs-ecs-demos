"""
	Class: Scan

		Scan Component

	Remarks:

		This Component stores the Scan
		information for a target


"""
class_name Scan
extends Node

export var RADIUS: int = 10
export (NodePath) var TARGET
export (NodePath) var COLLISION_SHAPE
export (NodePath) var RAYCAST

var radius: int
var target: Area2D
var cshape: CollisionShape2D
var found: bool
var ray: RayCast2D
var pos: Vector2
var dist: int

func _ready() -> void:

	Logger.trace("[Scan] _ready")

	if !RAYCAST:
		Logger.error("- entity [{0}] is missing a RayCast2D node".format([get_parent().get_parent().name]))
		get_tree().quit()

	if RADIUS:
		radius = RADIUS

	if TARGET:
		target = get_node(TARGET)

	if RAYCAST:
		ray = get_node(RAYCAST)

	# get collision shape on entity

	if COLLISION_SHAPE:
		cshape = get_node(COLLISION_SHAPE)

