extends Node
class_name Aim

export (Array) var RAYCAST_NODES

var found: bool
var data: Dictionary

var nodes
var rays

func _ready() -> void:

	if RAYCAST_NODES:	nodes = RAYCAST_NODES

	if nodes:

		rays = []

		for i in nodes.size():
			rays.append(get_node(nodes[i]))


