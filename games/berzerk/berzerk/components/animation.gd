extends Node

export (String) var ANIMATION = 'idle'
export (int) var SPEED = 1

var animation = ""
var last_animation = ""
var speed = 1


func _ready():

	if ANIMATION:			animation = ANIMATION
	if SPEED:				speed = SPEED