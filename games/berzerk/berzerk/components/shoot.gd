extends Component
class_name Shoot

export var SHOT_DELAY : float = 30.0
export var SHOT_SPEED : int = 1

var shot_delay: float = 30.0
var shot_speed: int = 1
var can_shoot: bool = false

var delay: float = 0.0


func _ready():
	
	if (SHOT_DELAY):	shot_delay = SHOT_DELAY
	if (SHOT_SPEED):	shot_speed = SHOT_SPEED
	
	# in milliseconds
	shot_delay = shot_delay / 1000
	
	# default is to be able to shoot
	can_shoot = true
	
	
	

