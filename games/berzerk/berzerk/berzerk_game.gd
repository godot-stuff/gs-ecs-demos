extends Node2D

export var draw_zones : bool = false

# a place to store all entities
onready var entity_root = $Entities

# timer for showcasing different menus
onready var showcase_timer = $Timers/ShowcaseTimer

# timer for waiting to resume showcase timer
onready var menu_timer = $Timers/MenuTimer

# timer to determine when otto shows up
onready var otto_timer = $Timers/OttoTimer

# simple way to store the pillars for maze creation
onready var pillar_positions = [
	$UI/Run/WorkRoom/Pillars/Pillar01.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar02.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar03.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar04.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar05.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar06.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar07.global_position,
	$UI/Run/WorkRoom/Pillars/Pillar08.global_position,
]

# simple array to hold the lives on the ui
onready var lives_icons = [

	$UI/Run/Lives/Lives1,
	$UI/Run/Lives/Lives2,
	$UI/Run/Lives/Lives3,
	$UI/Run/Lives/Lives4,
]

# game variables
var score : int
var lives : int
var bonus : int
var room_pos : Vector2 = Vector2(7, 7)
var total_robots : int
var robots_killed : int 
var robotbonus : bool
var last_exit : int = Berzerk.GAME_EXIT_RIGHT

# game states
var game_run_state : int
var game_room_state : int

# holder for the intruder entity
var intruder : Entity
var killed: Entity
# holder for the otto entity
var otto : Entity
var otto_mover : Entity

# holder for the different zones where robots can spawn
var spawn_zones = []

# simple array to roll through the different ui scenes
var _showcase = [

	Berzerk.GAME_STATE_TITLE,
	Berzerk.GAME_STATE_SCORES,
	Berzerk.GAME_STATE_ABOUT,
	Berzerk.GAME_STATE_TITLE,
	Berzerk.GAME_STATE_SCORES,
	Berzerk.GAME_STATE_ABOUT,
	Berzerk.GAME_STATE_TITLE,
	Berzerk.GAME_STATE_SCORES,
	Berzerk.GAME_STATE_ABOUT,
	Berzerk.GAME_STATE_DEMO,
]

# robot level colors
var _robot_colors = [

	Color.yellow,
	Color.red,
	Color.lightblue,
	Color.lightgreen,
	Color.purple,
	Color.yellow,
	Color.white,
	Color.gold
]

# high score table used
var hi_scores = [

	{ "name": "AAA", "score": 10000 },
	{ "name": "BBB", "score": 5000 },
	{ "name": "CCC", "score": 4000 },
	{ "name": "DDD", "score": 3000 },
	{ "name": "EEE", "score": 2000 },
]

var showcase_index : int = 0

func _add_entity(entity : Entity) -> Entity:
	Logger.trace("[berzerk_game] _add_entity")
	entity_root.add_child(entity)
	return entity
	

func _clear_entities() -> void:
	Logger.trace("[berzerk_game] _clear_entities")
	$Systems/Run/RobotShootSystem.robot_laser_pool.kill_all()
	$Systems/Run/IntruderShootSystem.laser_pool.kill_all()
	for entity in $Entities.get_children():
		ECS.remove_entity(entity)
	ECS.rebuild()
	
	
func _create_robots():
	Logger.trace("[berzerk_game] _game_state_boot")
	
	total_robots = (randi() % 4) + 2
	
	var _spawn_bag = []
	
	for _z in spawn_zones.size():
		if (_get_intruder_zone() != _z):
			_spawn_bag.append(_z)
	
	for _spawn in total_robots:
		
		var _z = randi() % _spawn_bag.size()
		var _zone = _spawn_bag[_z]
		_spawn_bag.remove(_z)

		var _x : int = randi() % 54
		var _y : int = randi() % 54

		var _pos = Vector2(_x, _y) + spawn_zones[_zone].position

		var _robot = _add_entity(Berzerk.Robot.instance())
		_robot.global_position = _pos
		_robot.add_component(Debug.new())
		
		
func _get_intruder_zone():
	
	if (last_exit == Berzerk.GAME_EXIT_TOP): return 12
	if (last_exit == Berzerk.GAME_EXIT_BOTTOM): return 2
	if (last_exit == Berzerk.GAME_EXIT_LEFT): return 9
	if (last_exit == Berzerk.GAME_EXIT_RIGHT): return 5
	
	return -1
	
	
func _create_room():
	for pillar_index in range(8):
		_create_room_wall(pillar_index)	


func _create_room_wall(index : int):
	var _dir = randi() % 4
	var _wall = _add_entity(Berzerk.Wall.instance()) as Entity
	_wall.rotation_degrees = 90 * _dir
	var dir = Vector2(1, 0).rotated(deg2rad(_wall.rotation_degrees))
	_wall.global_position = pillar_positions[index] + (dir * 48)

	
func _draw():
	
	if (draw_zones):
		for _rect in spawn_zones:
			draw_rect(_rect, Color.yellow, false)
	

func _game_room_start() -> void:
	Logger.trace("[berzerk_game] _game_room_start")
	game_room_state = Berzerk.GAME_ROOM_ENTER
	_clear_entities()
	yield(get_tree(), "idle_frame")
	robotbonus = false
	robots_killed = 0
	bonus = 0

func _game_room_enter() -> void:
	Logger.trace("[berzerk_game] _game_room_enter")
	
	# add intruder at correct door
	intruder = Berzerk.Intruder.instance() as Entity
	_add_entity(intruder)
	var _pos = _get_start_position(last_exit)
	intruder.global_position = _pos
	intruder.get_component("animation").animation = "idle_left"
	
	# create walls
	_create_room()
	
	# add robots based on level
	_create_robots()

	# setup otto appearence	
	otto_timer.wait_time = total_robots * 3
	otto_timer.start()

	# start room processing
	game_room_state = Berzerk.GAME_ROOM_PROCESS
	
	
func _get_start_position(exit_: int):
	Logger.trace("[berzerk_game] _get_start_position")
	
	var _pos = Vector2(0, 0)
	
	if (exit_ == Berzerk.GAME_EXIT_BOTTOM): _pos = Vector2(256, 75)
	if (exit_ == Berzerk.GAME_EXIT_TOP): _pos = Vector2(256, 305)
	if (exit_ == Berzerk.GAME_EXIT_RIGHT): _pos = Vector2(50, 180)
	if (exit_ == Berzerk.GAME_EXIT_LEFT): _pos = Vector2(470, 180)
	
	return _pos
	

func _game_room_process() -> void:
	ECS.update("run")
	

func _game_room_exit() -> void:
	Logger.trace("[berzerk_game] _game_room_exit")
	if (otto_mover):
		otto_mover.remove_child(otto)
	otto = null
	otto_mover = null
	_clear_entities()
	game_room_state = Berzerk.GAME_ROOM_STOP


func _game_room_stop() -> void:
	Logger.trace("[berzerk_game] _game_room_stop")
	game_room_state = Berzerk.GAME_ROOM_START
	
	if (lives < 1):
		game_run_state = Berzerk.GAME_RUN_STOP


func _game_run_start() -> void:
	Logger.trace("[berzerk_game] _game_run_start")
	lives = 3
	score = 0
	_updatelives()
	_updatescore()
	game_run_state = Berzerk.GAME_RUN_PROCESS
	game_room_state = Berzerk.GAME_ROOM_START


# default run state in a room
func _game_run_process() -> void:
	
	_updatescore()
		
	match game_room_state:
		
		Berzerk.GAME_ROOM_START:
			_game_room_start()
						
		Berzerk.GAME_ROOM_ENTER:
			_game_room_enter()
						
		Berzerk.GAME_ROOM_PROCESS:
			_game_room_process()
						
		Berzerk.GAME_ROOM_EXIT:
			_game_room_exit()
						
		Berzerk.GAME_ROOM_STOP:
			_game_room_stop()
						
	
func _game_run_stop() -> void:
	Logger.trace("[berzerk_game] _game_run_stop")
	_clear_entities()
	Berzerk.change_state(Berzerk.GAME_STATE_BOOT)
	
	
func _game_state_about() -> void:
	Logger.trace("[berzerk_game] _game_state_boot")
	VisualServer.set_default_clear_color(Color.black)
	_show_ui($UI/About)
	$UI/About/Title.grab_focus()
	
func _game_state_boot() -> void:
	Logger.trace("[berzerk_game] _game_state_boot")
	VisualServer.set_default_clear_color(Color.black)
	Berzerk.change_state(Berzerk.GAME_STATE_TITLE)
	

func _game_state_quit() -> void:
	Logger.trace("[berzerk_game] _game_state_quit")
	get_tree().quit()
	

func _game_state_run() -> void:
	Logger.trace("[berzerk_game] _game_state_run")
	_show_ui($UI/Run)
	$UI/Run/Title.grab_focus()
	game_run_state = Berzerk.GAME_RUN_START


func _game_state_scores() -> void:
	Logger.trace("[berzerk_game] _game_state_scores")
	_show_ui($UI/Scores)
	$UI/Scores/Title.grab_focus()
	

func _game_state_demo() -> void:
	Logger.trace("[berzerk_game] _game_state_demo")
	_show_ui($UI/Demo)
	

func _game_state_title() -> void:
	Logger.trace("[berzerk_game] _game_state_title")
	_show_ui($UI/Title)
	$UI/Title/Menu/Start.grab_focus()
	intruder = Berzerk.Intruder.instance() as Entity
	_add_entity(intruder)
	intruder.global_position = Vector2(256, 160)
	intruder.get_component("animation").animation = "idle_left"
	

func _hide_ui_all() -> void:
	Logger.trace("[berzerk_game] _hide_ui_all")
	_hide_ui($UI/Title)
	_hide_ui($UI/Run)
	_hide_ui($UI/About)
	_hide_ui($UI/Scores)
	_hide_ui($UI/Demo)
		
	
func _hide_ui(node : Node) -> void:
	Logger.trace("[berzerk_game] _hide_ui")
	node.hide()
	
	
func _init() -> void:
	Logger.trace("[berzerk_game] _init")
	
	# create spawn zones
	
	for _r in 3:
		for _c in 5:
			
			var _s = 54
			var _m = 40
			var _pos_top_x = _m + (_s * _c) + (_m * _c)
			var _pos_top_y = 74 + (_s * _r) + (_m * _r)
			var _size = Vector2(_s, _s)
			var _pos = Vector2(_pos_top_x, _pos_top_y)
			var _rect : Rect2 = Rect2(_pos, _size)
			spawn_zones.append(_rect)
			
	randomize()
	

func _on_game_state_changed(state : int) -> void:
	
	Logger.trace("[berzerk_game] _on_game_state_changed")
	Logger.debug("- new state is %s" % [state])
	
	_hide_ui_all()
	_clear_entities()
	
	yield(get_tree(), "idle_frame")
	
	match state:
		
		Berzerk.GAME_STATE_ABOUT:
			_game_state_about()

		Berzerk.GAME_STATE_BOOT:
			_game_state_boot()

		Berzerk.GAME_STATE_DEMO:
			_game_state_demo()

		Berzerk.GAME_STATE_QUIT:
			_game_state_quit()

		Berzerk.GAME_STATE_RUN:
			_game_state_run()

		Berzerk.GAME_STATE_SCORES:
			_game_state_scores()

		Berzerk.GAME_STATE_TITLE:
			_game_state_title()

	
func _on_intruder_exit_room(exit: int) -> void:
	Logger.trace("[berzerk_game] _on_intruder_room_exit")
	last_exit = exit
	score += bonus
	game_room_state = Berzerk.GAME_ROOM_EXIT
	
	
func _on_LinkButton_pressed() -> void:
	OS.shell_open("https://gitlab.com/godot-stuff/gs-ecs")


func _on_Start_pressed():
	Logger.trace("[berzerk_game] _on_Start_pressed")
	showcase_timer.stop()
	menu_timer.stop()
	Berzerk.change_state(Berzerk.GAME_STATE_RUN)


func _on_Quit_pressed():
	Logger.trace("[berzerk_game] _on_Quit_pressed")
	Berzerk.change_state(Berzerk.GAME_STATE_QUIT)


func _on_Scores_pressed():
	Logger.trace("[berzerk_game] _onscores_pressed")
	showcase_timer.stop()
	menu_timer.stop()
	menu_timer.start()
	Berzerk.change_state(Berzerk.GAME_STATE_SCORES)


func _on_About_pressed():
	Logger.trace("[berzerk_game] _on_About_pressed")
	showcase_timer.stop()
	menu_timer.stop()
	menu_timer.start()
	Berzerk.change_state(Berzerk.GAME_STATE_ABOUT)


func _on_Title_pressed():
	Logger.trace("[berzerk_game] _on_Title_pressed")
	showcase_timer.stop()
	menu_timer.stop()
	menu_timer.start()
	Berzerk.change_state(Berzerk.GAME_STATE_BOOT)
	
	
func _on_intruder_killed():
	Logger.trace("[berzerk_game] _on_intruder_killed")
	$Audio/Killed.play()
	killed = _add_entity(Berzerk.KilledIntruder.instance()) as Entity
	killed.global_position = intruder.global_position
	$Systems/Run/OttoMovementSystem.target = killed
	ECS.remove_entity(intruder)
	yield(get_tree().create_timer(1.0), "timeout")
	lives += -1
	_updatelives()
	game_room_state = Berzerk.GAME_ROOM_EXIT
	
	
func _on_intruder_shoot():
	Logger.trace("[berzerk_game] _on_intruder_shoot")
	$Audio/Shoot.play()	


func _on_MenuTimer_timeout():
	Logger.trace("[berzerk_game] _on_MenuTimer_timeout")
	showcase_index = 0
	showcase_timer.start()
	Berzerk.change_state(Berzerk.GAME_STATE_BOOT)


func _on_OttoTimer_timeout():
	Logger.trace("[berzerk_game] _on_OttoTimer_timeout")
	otto_mover = _add_entity(Berzerk.OttoMover.instance())
#	otto = _add_entity(Berzerk.Otto.instance())
	otto = Berzerk.Otto.instance()
	otto_mover.add_child(otto)
	otto.get_component("animation").animation = "bounce"
	otto_mover.global_position = _get_start_position(last_exit)
	$Systems/Run/OttoMovementSystem.target = intruder
	$Systems/Run/OttoMovementSystem.enabled = true


func _on_robot_shoot():
	Logger.trace("[berzerk_game] _on_robot_shoot")
	$Audio/Shoot.play()	
	
	
func _on_robot_killed(robot : Entity):
	Logger.trace("[berzerk_game] _on_robot_killed")
	score += 50
	bonus += 10
	robots_killed += 1
	$Audio/Destroy.play()
	var _killed_robot = _add_entity(Berzerk.KilledRobot.instance())
	_killed_robot.get_component("animation").animation = "killed"
	_killed_robot.get_component("animation").speed = 12
	_killed_robot.global_position = robot.global_position
	ECS.remove_entity(robot)
	
	if (robots_killed >= total_robots):
		robotbonus = true


func _on_ShowcaseTimer_timeout():
	Logger.trace("[berzerk_game] _on_ShowcaseTimer_timeout")
	showcase_index += 1
	
	if (showcase_index >= _showcase.size()):
		showcase_index = 0
				
	Berzerk.change_state(_showcase[showcase_index])
	showcase_timer.start()
	

func _process(delta) -> void:
	
	ECS.update("common")
	
	match Berzerk.game_state:
		
		Berzerk.GAME_STATE_RUN:
			_process_game_state_run(delta)
			
	ECS.update("debug")
	update()
	

func _process_game_state_run(delta) -> void:

	match game_run_state:
		
		Berzerk.GAME_RUN_START:
			_game_run_start()
						
		Berzerk.GAME_RUN_PROCESS:
			_game_run_process()
					
		Berzerk.GAME_RUN_STOP:
			_game_run_stop()
			
	
func _ready() -> void:
	Logger.trace("[berzerk_game] _ready")
	
	_hide_ui_all()
	
	Berzerk.connect("game_state_changed", self, "_on_game_state_changed")
	Berzerk.connect("intruder_exit_room", self, "_on_intruder_exit_room")
	Berzerk.connect("intruder_killed", self, "_on_intruder_killed")
	Berzerk.connect("robot_shoot", self, "_on_robot_shoot")
	Berzerk.connect("intruder_shoot", self, "_on_intruder_shoot")
	Berzerk.connect("robot_killed", self, "_on_robot_killed")
	
	showcase_timer.start()
	
	Berzerk.change_state(Berzerk.GAME_STATE_BOOT)
	
	

func _show_ui(node : Node) -> void:
	Logger.trace("[berzerk_game] _show_ui")
	node.show()
	
	
func _updatescore():
	$UI/Run/Score_.text = "%s" % [score]
	$UI/Run/Bonus_.text = ""
	
	if (robotbonus):
		$UI/Run/Bonus_.text = "BONUS %s" % [bonus]

func _updatelives():
	
	# do not include showing last life, so in
	# effect you can show up to 5 lives
	for _index in lives_icons.size():
		lives_icons[_index].hide()
		if (_index < lives - 1):
			lives_icons[_index].show()
			


