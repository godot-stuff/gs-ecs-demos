extends DebugNode


func on_draw():

	var _shoot = entity.get_component("shoot") as Shoot

	draw_text(entity.position + Vector2(-50,-25), ["can_shoot: %s" % [_shoot.can_shoot]])