extends DebugNode


func on_draw():

	var _system = ECS.systems['robotshootsystem'] as RobotShootSystem
	
	var _text = []
	
	_text.append("total: %s" % [_system.robot_laser_pool.get_size()])
	_text.append("available: %s" % [_system.robot_laser_pool.get_available_size()])
	_text.append("active: %s" % [_system.robot_laser_pool.get_active_size()])

	draw_text(entity.position + Vector2(0,25), _text)