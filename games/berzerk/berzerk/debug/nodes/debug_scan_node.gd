extends DebugNode

func on_draw() -> void:

	var _scan = entity.get_component("scan") as Scan

	if _scan.found:
		draw_line(entity.global_position, _scan.pos, DEBUG_COLOR_LINE_4, 1.0, false)

	text.clear()

	text.append("dist: {0}".format([_scan.dist]))
	text.append("hello world")
	var y = 0;

	draw_text(entity.position + Vector2(0, 20), text, DEBUG_COLOR_LINE_1)
