extends DebugNode


func on_draw():

	var _aim = entity.get_component("aim") as Aim

	var _rays = _aim.data.rays

	for i in _rays.size():
		var _ray = _rays[i]
		var _start = entity.global_position
		var _end = _start + _ray.cast_to.rotated(deg2rad(_ray.rotation_degrees))
#		Logger.trace("{2}:{0}:{1}".format([_start, _end, entity.name]))
		draw_line(_start, _end, DEBUG_COLOR_LINE_1, 1.0, false)

	if _aim.data.found:

		for i in _rays.size():
			var _ray = _rays[i]
			if _ray.is_colliding:
				draw_line(entity.global_position, _ray.collision_point, DEBUG_COLOR_LINE_2, 1.0, false)
