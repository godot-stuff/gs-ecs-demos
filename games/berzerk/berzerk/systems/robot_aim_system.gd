extends GameSystem
class_name RobotAimSystem


func on_process(entities, delta):

	for entity in entities:

		var _aim = entity.get_component("aim") as Aim

		var _found = false
		var _rays = []

		_aim.data = {}

		if _aim.rays:

			for i in _aim.rays.size():

				var _ray = _aim.rays[i] as RayCast2D
				_ray.global_position = entity.global_position
				var _ray_data = { "rotation_degrees": _ray.rotation_degrees, "cast_to": _ray.cast_to, "is_colliding": _ray.is_colliding(), "collision_point": _ray.get_collision_point(), "collider": _ray.get_collider() }

				if _ray.is_colliding():
					_found = true

				_rays.append(_ray_data)

		_aim.data = { "found": _found, "rays": _rays }

