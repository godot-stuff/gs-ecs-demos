extends System
class_name KillIntruderSystem

func on_process_entity(entity : Entity, delta):

	Berzerk.emit_signal("intruder_killed")
