"""
	A System to Kill Laser Entities
"""
extends System
class_name KillLaserSystem

func on_process_entity(entity : Entity, delta):

	entity.remove_component("kill")
	entity.remove_component("collide")
	var _laser = entity.get_component("laser") as Laser
	if (_laser.laser_pool):
		_laser.laser_pool.set_unavailable(entity)
