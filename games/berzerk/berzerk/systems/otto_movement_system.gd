extends System
class_name OttoMovementSystem

var target : Entity

func on_process_entity(entity_: Entity, delta_):
	
	if (target):
		
		entity_.get_component("moveto").target = target.global_position
		entity_.get_component("moveto").is_moving = true
	