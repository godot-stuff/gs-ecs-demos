extends System
class_name CollisionSystem

func on_process_entity(entity : Entity, delta):
	
	if (!entity.enabled):
		return

	var _collide = entity.get_component("collide") as Collide

	_collide.areas = entity.get_overlapping_areas()

	if _collide.areas.size() > 0:
		_collide.has_collided = true
		
