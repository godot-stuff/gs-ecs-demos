extends GameSystem

func on_process(entities, delta):

	for entity in entities:

		var _vel = entity.get_component("velocity")

		_vel.velocity += _vel.direction * _vel.speed * _vel.speed_factor
		entity.position += _vel.velocity * delta
		_vel.velocity = Vector2.ZERO
