extends System
class_name LaserCollisionSystem

func on_process_entity(entity : Entity, delta):

	var _collide = entity.get_component("collide") as Collide
	
	if (_collide.has_collided):
		
		# kill the laser
		Berzerk.kill_entity(entity)

		# kill anything else except walls		
		for area in _collide.areas:
			Logger.debug("- %s has collided with the laser" % [area.name])
			
			if (area.has_component("wall")):
				Logger.debug("- you can't kill walls silly")
				continue
				
			Berzerk.kill_entity(area)
