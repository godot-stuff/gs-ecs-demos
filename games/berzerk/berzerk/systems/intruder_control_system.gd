extends GameSystem

func on_process(entities, delta):

	for entity in entities:

		var _animation = entity.get_component("animation")
		var _intruder = entity.get_component("intruder") as Intruder
		var _velocity = entity.get_component("velocity")

		var _anim = "idle_right"
		var _dir = Vector2.ZERO
		
		_intruder.shoot_dir = 0.0

		if _intruder.facing_left:
			_anim = "idle_left"

		if Input.is_action_pressed("right"):
			_anim = "run_right"
			_intruder.facing_left = false
			_dir = Vector2(_intruder.hspeed, 0)

		if Input.is_action_pressed("left"):
			_anim = "run_left"
			_intruder.facing_left = true
			_dir = Vector2(-_intruder.hspeed, 0)

		if Input.is_action_pressed("up"):
			_anim = "run_right"
			if _intruder.facing_left:
				_anim = "run_left"
			_dir = _dir + Vector2(0, -_intruder.vspeed)

		if Input.is_action_pressed("down"):
			_anim = "run_right"
			if _intruder.facing_left:
				_anim = "run_left"
			_dir = _dir + Vector2(0, _intruder.vspeed)
			
		if (Input.is_action_pressed("fire")):
			
			_dir = Vector2.ZERO
			
			if (Input.is_action_pressed("up")):
				_anim = "shoot_right_up"
				if (_intruder.facing_left):
					_anim = "shoot_left_up"
				_intruder.shoot_dir = 360.0

			if (Input.is_action_pressed("down")):
				_anim = "shoot_right_down"
				if (_intruder.facing_left):
					_anim = "shoot_left_down"
				_intruder.shoot_dir = 180.0

			if (Input.is_action_pressed("left")):
				_anim = "shoot_left_mid"
				_intruder.shoot_dir = 270.0

			if (Input.is_action_pressed("right")):
				_anim = "shoot_right_mid"
				_intruder.shoot_dir = 90.0
				
			if (Input.is_action_pressed("up") and Input.is_action_pressed("right")):
				_anim = "shoot_up_right"
				_intruder.shoot_dir = 45.0
				
			if (Input.is_action_pressed("up") and Input.is_action_pressed("left")):
				_anim = "shoot_up_left"
				_intruder.shoot_dir = 315.0
				
			if (Input.is_action_pressed("down") and Input.is_action_pressed("right")):
				_anim = "shoot_down_right"
				_intruder.shoot_dir = 135.0
				
			if (Input.is_action_pressed("down") and Input.is_action_pressed("left")):
				_anim = "shoot_down_left"
				_intruder.shoot_dir = 225.0
				

		_animation.animation = _anim
		_velocity.direction = _dir

