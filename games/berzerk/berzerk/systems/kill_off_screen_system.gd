"""
	A System to Kill Entities Off Screen
"""
extends System
class_name KillOffScreenSystem

var _frame : int = 0

func on_process(entities, delta):
	
	
	# only check every 10 frames
	
	_frame += 1
	
	if (_frame < 100):
		return
		
	_frame = 0
	
	for entity in entities:
		
		var _off_screen = false
		
		if (entity.global_position.x < 0) : _off_screen = true
		if (entity.global_position.y < 0) : _off_screen = true
		if (entity.global_position.x > 512) : _off_screen = true
		if (entity.global_position.y > 448) : _off_screen = true
		
		
		if (_off_screen):
			
			var _kill = Kill.new()
			_kill.name = "Kill"
			entity.add_component(_kill)		
