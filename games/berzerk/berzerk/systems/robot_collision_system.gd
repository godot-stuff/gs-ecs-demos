extends System
class_name RobotCollisionSystem

func on_process_entity(entity : Entity, delta):

	var _collide = entity.get_component("collide") as Collide
	
	if (_collide.has_collided):
		
		# kill the robot
		Berzerk.kill_entity(entity)

		# kill any other entity touching the robot		
		for area in _collide.areas:
			Logger.debug("- %s has collided with the robot" % [area.name])
			
			if (area.has_component("wall")):
				Logger.warn("- you can't kill walls silly")
				continue
				
			if (area.has_component("otto")):
				Logger.warn("- you can't kill otto silly")
				continue
				
			
			Berzerk.kill_entity(area)


			
