extends System
class_name IntruderExitSystem

func on_process_entity(entity : Entity, delta):
	
	if (entity.global_position.x < 20):
		Berzerk.emit_signal("intruder_exit_room", Berzerk.GAME_EXIT_LEFT)
		
	if (entity.global_position.x > 500):
		Berzerk.emit_signal("intruder_exit_room", Berzerk.GAME_EXIT_RIGHT)
		
	if (entity.global_position.y < 45):
		Berzerk.emit_signal("intruder_exit_room", Berzerk.GAME_EXIT_TOP)
			
	if (entity.global_position.y > 335):
		Berzerk.emit_signal("intruder_exit_room", Berzerk.GAME_EXIT_BOTTOM)
			