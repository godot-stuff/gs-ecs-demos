"""
	Class: ScanSystem

	Scan for a Target

	Remarks:

		This System will Scan for a Target Entity
		specified on the Scan Component.

"""
class_name ScanSystem
extends GameSystem

func on_process(entities, delta):

	for entity in entities:

		var _scan = entity.get_component("scan") as Scan

		_scan.found = false

		if _scan.target:
			_scan.ray.global_position = entity.global_position
			_scan.ray.cast_to = _scan.target.global_position - entity.global_position

		if _scan.ray.is_colliding():
#			Logger.trace("{0}".format([_scan.ray.get_collision_point()]))
			_scan.pos = _scan.ray.get_collision_point()
			_scan.found = true

		# calculate the distance
		_scan.dist = int(entity.position.distance_to(_scan.pos))
