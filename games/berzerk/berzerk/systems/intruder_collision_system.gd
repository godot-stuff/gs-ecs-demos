extends System
class_name IntruderCollisionSystem

func on_process_entity(entity : Entity, delta):

	var _collide = entity.get_component("collide") as Collide
	
	if (_collide.has_collided):
			
		var _kill = Kill.new()
		_kill.name = "Kill"
		entity.add_component(_kill)		
