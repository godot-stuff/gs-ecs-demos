extends GameSystem
class_name AnimationSystem

func on_process(entities, delta):

	for entity in entities:
		
		var _anim = entity.get_component("animation") #as Animation
		var _player = entity.get_node("AnimationPlayer") #as AnimationPlayer
		
		Logger.debug("- entity: %s" % [entity.name])
		Logger.debug("- animation: %s" % [_anim.animation])
		

		if _anim.animation != _anim.last_animation:

			_player.current_animation = _anim.animation
			_player.playback_speed = _anim.speed
			_anim.last_animation = _anim.animation
			

