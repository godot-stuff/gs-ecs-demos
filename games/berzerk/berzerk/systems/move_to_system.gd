"""
	System: MoveToSystem
		Moves an Entity to World Position

"""
extends System
class_name MoveToSystem

func on_process(entities, delta):

	for entity in entities:

		var moveto = entity.get_component("moveto") as MoveTo

		moveto.complete = false

		if (moveto.is_moving):
			var _dir = (moveto.target - entity.position).normalized()
			var _vel = _dir * moveto.speed * moveto.speed_factor * delta

			var distance_to = Vector2(abs(moveto.target.x - entity.position.x), abs(moveto.target.y - entity.position.y))

			if abs(_vel.x) > distance_to.x:
				_vel.x = distance_to.x * _dir.x
				moveto.is_moving = false
				moveto.complete = true

			if abs(_vel.y) > distance_to.y:
				_vel.y = distance_to.y * _dir.y
				moveto.is_moving = false
				moveto.complete = true

			entity.position += _vel

