extends GameSystem
class_name IntruderShootSystem


var laser_pool: LaserPool

var _fire : bool = false
var _fire_dir : float


func on_process_entity(entity : Entity, delta):
	
	var _intruder = entity.get_component("intruder") as Intruder
	var _shoot = entity.get_component("shoot") as Shoot
	
	_fire = false
	
	_shoot.delay += delta
	
	if (_shoot.delay > _shoot.shot_delay):
		_shoot.can_shoot = true

	Logger.fine("shot_delay %s" % [_shoot.shot_delay])
	Logger.fine("delay %s" % [_shoot.delay])
	Logger.fine("can_shoot %s" % [_shoot.can_shoot])
	
	if (! _shoot.can_shoot):
		return	
		
#	_shoot.can_shoot = false

	if (_intruder.shoot_dir > 0):
		
		_fire = true
		
		if (_fire):
												
			var laser = laser_pool.get_first_available() as Entity
		
			if (laser):
#				laser.rotation = PI
				laser.rotation_degrees = _intruder.shoot_dir + 90
				laser.global_position = entity.global_position
				laser.get_component("laser").laser_pool = laser_pool
				laser.get_component("velocity").speed = _shoot.shot_speed
				var dir = Vector2(1, 0).rotated(deg2rad(_intruder.shoot_dir - 90))
				laser.get_component("velocity").direction = dir
				
				# add collide component
				var _collide = Collide.new()
				_collide.name = "collide"
				laser.add_component(_collide)
				
				# move laser outside of robot range
				laser.global_position += dir * 18

				_shoot.delay = 0.0
				
				_shoot.can_shoot = false
				
				Berzerk.emit_signal("intruder_shoot")
				

func _ready():
	
	laser_pool = LaserPool.new(3, "intruder_laser", Berzerk.Laser)
	laser_pool.add_to_node(self)
	
	