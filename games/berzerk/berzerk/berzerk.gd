extends Node

onready var Intruder: PackedScene = preload("res://berzerk/entities/intruder.tscn")
onready var KilledIntruder: PackedScene = preload("res://berzerk/entities/killed_intruder.tscn")
onready var Robot: PackedScene = preload("res://berzerk/entities/robot.tscn")
onready var KilledRobot: PackedScene = preload("res://berzerk/entities/killed_robot.tscn")
onready var Laser: PackedScene = preload("res://berzerk/entities/laser.tscn")
onready var Wall: PackedScene = preload("res://berzerk/entities/wall.tscn")
onready var Otto: PackedScene = preload("res://berzerk/entities/otto.tscn")
onready var OttoMover: PackedScene = preload("res://berzerk/entities/otto_mover.tscn")


const COLLISION_LAYER_PLAYER = 0
const COLLISION_LAYER_ENEMY = 1
const COLLISION_LAYER_LASER = 2
const COLLISION_LAYER_OBJECT = 3
const COLLISION_LAYER_WALL = 4

const GAME_STATE_NONE = 00
const GAME_STATE_BOOT = 10
const GAME_STATE_TITLE = 20
const GAME_STATE_SCORES = 30
const GAME_STATE_RUN = 40
const GAME_STATE_WIN = 50
const GAME_STATE_LOSE = 55
const GAME_STATE_DEMO = 60
const GAME_STATE_ABOUT = 70
const GAME_STATE_QUIT = 99

const GAME_RUN_START = 00
const GAME_RUN_PROCESS = 50
const GAME_RUN_STOP = 99

const GAME_ROOM_START = 10
const GAME_ROOM_ENTER = 00
const GAME_ROOM_PROCESS = 50
const GAME_ROOM_EXIT = 99
const GAME_ROOM_STOP = 90

const GAME_EXIT_TOP = 1
const GAME_EXIT_BOTTOM = 2
const GAME_EXIT_LEFT = 3
const GAME_EXIT_RIGHT = 4


# when the game state has changed
signal game_state_changed(state)

# when the intruder has exited a room
signal intruder_exit_room(exit)

# when intruder has been killed
signal intruder_killed()

# when robot has been killed
signal robot_killed(entity)

# when a robot shoots
signal robot_shoot()

# when intruder shoots
signal intruder_shoot()

# game state
var game_state setget no_setter, _get_game_state


func change_state(state : int) -> void:
	Logger.trace("[berzerk] change_state")
	game_state = state
	emit_signal("game_state_changed", game_state)
	

func _get_game_state():
	return game_state

	
func kill_entity(entity_ : Entity):
	Logger.trace("[berzerk] kill_entity")
	var _kill = Kill.new()
	_kill.name = "kill"
	entity_.add_component(_kill)		
	
	var _velocity = entity_.get_component("velocity") as Velocity
	_velocity.direction = Vector2.ZERO


func no_setter(value):
	return


func no_getter():
	return

