extends Node2D

onready var intruder = $Intruder
onready var otto = $Otto
onready var otto_mover = $OttoMover

func _ready():
	remove_child(otto)
	otto_mover.add_child(otto)
	otto.get_component("animation").animation = "bounce"
	otto_mover.get_component("moveto").target = intruder.global_position
	otto_mover.get_component("moveto").is_moving = true
	$OttoMovementSystem.target = $Intruder

func _process(delta):
	ECS.update()
	