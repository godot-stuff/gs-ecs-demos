extends Node

onready var intruder = get_node("Intruder")
onready var otto = $OttoPosition/Otto
onready var robot = get_node("Robot")

func _process(delta):
	ECS.update()


func _on_IntruderIdleLeft_pressed():
	intruder.get_component("animation").animation = "idle_left"


func _on_IntruderIdleRight_pressed():
	intruder.get_component("animation").animation = "idle_right"


func _on_IntruderRunLeft_pressed():
	intruder.get_component("animation").animation = "run_left"


func _on_IntruderRunRight_pressed():
	intruder.get_component("animation").animation = "run_right"


func _on_OttoMove_pressed():
	otto.get_component("animation").animation = "move"


func _on_OttoBounce_pressed():
	otto.get_component("animation").animation = "bounce"


func _on_RobotIdle_pressed():
	robot.get_component("animation").animation = "idle"


func _on_RobotMoveLeft_pressed():
	robot.get_component("animation").animation = "move_left"


func _on_RobotMoveRight_pressed():
	robot.get_component("animation").animation = "move_right"


func _on_RobotMoveUp_pressed():
	robot.get_component("animation").animation = "move_up"


func _on_RobotMoveDown_pressed():
	robot.get_component("animation").animation = "move_down"
