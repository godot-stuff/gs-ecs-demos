extends Node


onready var _otto = get_node("Otto")


func _process(delta):
	ECS.update()


func _ready():
	_otto.get_component("velocity").direction = Vector2(1, 1)

