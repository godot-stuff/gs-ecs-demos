extends Node

func _physics_process(delta: float) -> void:
	ECS.update("physicsprocesssystem")

func _process(delta: float) -> void:
	ECS.update("processsystem")

func _ready():
	pass
