extends Node2D

var robot_laser_pool: LaserPool
var killed = false


func _process(delta: float) -> void:
#	Logger.debug("- killed : %s" % [killed])
	ECS.update("common")
	ECS.update("systems")
	ECS.update("debug")


func _ready():
	randomize()
	Berzerk.connect("intruder_killed", self, "_on_intruder_killed")
	
	
func _on_intruder_killed():
	killed = true
	yield(get_tree().create_timer(1.0), "timeout")
	$Intruder.global_position = Vector2(256, 180)
	$Intruder.get_component("animation").animation = "idle_left"
	killed = false
